package com.example.rabbitmq;


import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMqConfig {

    @Bean
    public Queue Queue1(){
        return new Queue("myQueue");
    }

    @Bean
    public Queue Queue2(){
        return new Queue("myQueue2");
    }
}
