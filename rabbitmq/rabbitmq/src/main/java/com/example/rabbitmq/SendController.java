package com.example.rabbitmq;


import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
public class SendController {
    @Autowired
    private AmqpTemplate amqpTemplate;

    @RequestMapping("/send")
    public void send(){
        amqpTemplate.convertAndSend("myQueue","你好");
        amqpTemplate.convertAndSend("myQueue2","你好myQueue2");
    }

}
