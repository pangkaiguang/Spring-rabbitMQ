package com.example.rabbitmq;


import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class Receiver {

    @RabbitHandler
    @RabbitListener(queues = "myQueue")
    public void receiver(String msg){
        System.out.println("收到消息：" + msg);
    }

    @RabbitHandler
    @RabbitListener(queues = "myQueue2")
    public void receiver1(String msg){
        System.out.println("收到消息：" + msg);
    }
}
